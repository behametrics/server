from collections import defaultdict
from collections import namedtuple
from collections import OrderedDict

_InputEventField = namedtuple('_InputEventField', ['name', 'required'])

_COMMON_FIELDS = [
    ('input', True),
    ('session_id', True),
    ('timestamp', True),
    ('timestamp_precision', False),
]

_CONTENT_FIELDS_PER_INPUT = {
    'cursor': [
        ('event_type', True),
        ('button', True),
        ('x', True),
        ('y', True),
    ],
    'wheel': [
        ('button', True),
        ('delta_x', True),
        ('delta_y', True),
        ('delta_z', False),
        ('delta_unit', False),
        ('cursor_x', True),
        ('cursor_y', True),
    ],
    'keyboard': [
        ('event_type', True),
        ('key_code', True),
        ('key_name', True),
    ],
    'touch': [
        ('event_type', True),
        ('event_type_detail', True),
        ('pointer_id', True),
        ('x', True),
        ('y', True),
        ('pressure', False),
        ('size', False),
        ('touch_major', False),
        ('touch_minor', False),
        ('raw_x', False),
        ('raw_y', False),
    ],
    'sensor_accelerometer': [
        ('x', True),
        ('y', True),
        ('z', True),
    ],
    'sensor_linear_acceleration': [
        ('x', True),
        ('y', True),
        ('z', True),
    ],
    'sensor_gyroscope': [
        ('x', True),
        ('y', True),
        ('z', True),
    ],
    'sensor_magnetic_field': [
        ('x', True),
        ('y', True),
        ('z', True),
    ],
    'metadata': [
        ('logger_platform', True),
    ],
}


def _get_fields_dict(fields_):
    return OrderedDict((elem[0], _InputEventField(*elem)) for elem in fields_)


COMMON_FIELDS = _get_fields_dict(_COMMON_FIELDS)

CONTENT_FIELDS_PER_INPUT = defaultdict(OrderedDict)
for name, fields_ in _CONTENT_FIELDS_PER_INPUT.items():
    CONTENT_FIELDS_PER_INPUT[name] = _get_fields_dict(fields_)
