from abc import ABC

from bson import ObjectId
from pymongo import MongoClient

from server.config import Configuration

_client = MongoClient(Configuration.MONGO_URI)


def get_events_dao(client=_client):
    return EventsDao(client=client)


def get_sessions_dao(client=_client):
    return SessionsDao(client=client)


class Dao(ABC):

    def __init__(self, collection_name, client=_client):
        self._db = client.get_database()
        self._collection_name = collection_name

    @property
    def _collection(self):
        return self._db.get_collection(self._collection_name)

    @staticmethod
    def _stringify_oid(obj):
        if obj is None:
            return obj

        obj['_id'] = str(obj['_id'])
        return obj

    @staticmethod
    def _wrap_oid(obj):
        if '_id' in obj:
            obj['_id'] = ObjectId(obj['_id'])

        return obj

    def save(self, data):
        if isinstance(data, list):
            return self.save_batch(data)
        else:
            return self.save_one(data)

    def save_batch(self, obj_list):
        self._collection.insert_many(obj_list)
        return [self._stringify_oid(obj) for obj in obj_list]

    def save_one(self, json):
        self._collection.insert_one(json)
        return self._stringify_oid(json)

    def get(self, query=None):
        if query is None:
            query = {}
        return [self._stringify_oid(i) for i in
                self.get_cursor(self._wrap_oid(query))]

    def get_cursor(self, query=None):
        if query is None:
            query = {}
        return self._collection.find(self._wrap_oid(query))

    def get_one(self, query=None):
        if query is None:
            query = {}
        return self._stringify_oid(
            self._collection.find_one(self._wrap_oid(query)))

    def update_one(self, update, criteria=None, **kwargs):
        if criteria is None:
            criteria = {}
        return self._collection.update_one(criteria, update, **kwargs)


class EventsDao(Dao):
    """
    Data access object for input events.
    """

    def __init__(self, client=_client):
        super().__init__(client=client, collection_name='events')
    
    def aggregate(self, pipeline):
        return self._collection.aggregate(pipeline)


class SessionsDao(Dao):
    """
    Data access object for sessions.
    """

    def __init__(self, client=_client):
        super().__init__(client=client, collection_name='sessions')
