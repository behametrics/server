from json import JSONDecodeError

from flask import Blueprint
from flask import jsonify
from webargs import ValidationError
from werkzeug.exceptions import UnprocessableEntity

blueprint = Blueprint('error', __name__)


class NoMatchingData(UnprocessableEntity):

    def __init__(self, input_name):
        super().__init__()
        self.input_name = input_name

    def make_response(self):
        return make_error_response(
            message=f"No matching data for input '{self.input_name}'.",
            error_code='request/no-matching-data',
            detail=self.input_name,
            code=422)


def make_error_response(message, error_code, code, detail=None):
    return jsonify({
        'status': code,
        'code': error_code,
        'message': message,
        'detail': detail
    }), code


@blueprint.app_errorhandler(JSONDecodeError)
def handle_json_decode_error(e: JSONDecodeError):
    return make_error_response(
        message='Unable to parse the specified JSON.',
        error_code='request/illegal-json',
        detail=str(e),
        code=400)


@blueprint.app_errorhandler(422)
def handle_validation_error(e: UnprocessableEntity):
    exc = getattr(e, 'exc', e)

    if isinstance(exc, ValidationError):
        return make_error_response(
            message='Argument did not pass validation.',
            error_code='request/validation-failure',
            detail=exc.messages,
            code=422)

    if isinstance(exc, NoMatchingData):
        return e.make_response()

    return make_error_response(
        message='Invalid request.',
        error_code='request/unknown-error',
        code=422)
