from server import fields
from server.dao import get_events_dao
from server.util import normalize_input_name


def fetch_for_query(query):
    """Retrieve input events for the specified query."""
    return get_events_dao().get(query=query)


def save_data(data):
    """Store the data into the database collection for input events."""
    get_events_dao().save(data)


def create_csv(query, columns, separator=',', replacement_for_separator=';'):
    """
    Returns a generator that yields rows of a CSV file, given a MongoDB query
    matching data, the columns to be included and the field separator.
    
    `columns` is a tuple of two lists - names of common fields to be included,
    and names of fields inside the `content` sub-document to be included.
    
    The first row yielded is always the CSV header. Subsequent rows are values
    for the corresponding field (column) names.
    
    `separator`s inside field values are replaced with
    `replacement_for_separator`.
    """
    def get_field_value(document, column_name):
        field_value = document.get(column_name, None)
        field_value_str = (
            str(field_value).replace(separator, replacement_for_separator))
        
        return field_value_str if field_value is not None else ''
    
    cursor = get_events_dao().get_cursor(query)
    
    csv_columns = get_unique_column_names(*columns)
    yield f'{separator.join(csv_columns)}\n'
    
    columns_common, columns_content = columns
    
    for document in cursor:
        values_common = [
            get_field_value(document, column_name)
            for column_name in columns_common]
        
        if 'content' not in document:
            document['content'] = {}
        
        values_content = [
            get_field_value(document['content'], column_name)
            for column_name in columns_content]
        
        row = values_common + values_content
        yield f'{separator.join(row)}\n'


def get_query_for_input(query, input_name):
    """
    Return a query matching the specified input name and fields required by the
    input.
    
    The original `query` is merged with fields added by this function,
    overriding original fields having the same key.
    """
    return dict(query, **get_match_query(normalize_input_name(input_name)))


def get_columns_for_input(input_name):
    """
    Return a list of column names for the given input name.
    
    The names are taken from the top-level keys and keys inside the `content`
    key of each matching document. The names `'_id'` and `'content'` are
    excluded.
    
    A particular input may have a corresponding list of required and optional
    column names specified in the `fields` module. The column names are then
    sorted according to the field order. The remaining columns not specified in
    `fields` are appended and sorted alphabetically.
    
    If the input name does not exist, an empty list is returned.
    """
    if input_name is None:
        return None
    
    input_name = normalize_input_name(input_name)
    
    # Based on: https://stackoverflow.com/a/43570730
    cursor = get_events_dao().aggregate([
        {'$match': get_match_query(input_name)},
        {'$project': {
            'commonFields': {'$objectToArray': '$$ROOT'},
            'contentFields': {'$objectToArray': '$$ROOT.content'},
        }},
        {'$unwind': '$commonFields'},
        {'$unwind': '$contentFields'},
        {'$group': {
            '_id': None,
            'commonFieldNames': {'$addToSet': '$commonFields.k'},
            'contentFieldNames': {'$addToSet': '$contentFields.k'},
        }},
    ])
    
    try:
        columns_dict = cursor.next()
    except StopIteration:
        return None
    
    _safe_remove(columns_dict['commonFieldNames'], '_id')
    _safe_remove(columns_dict['commonFieldNames'], 'content')
    _safe_remove(columns_dict['contentFieldNames'], '_id')
    
    common_columns = _sort_columns(
        columns_dict['commonFieldNames'], fields.COMMON_FIELDS)
    content_columns = _sort_columns(
        columns_dict['contentFieldNames'],
        fields.CONTENT_FIELDS_PER_INPUT[input_name])
    
    return common_columns, content_columns


def get_unique_column_names(columns_common, columns_content):
    """
    Return a single list of column names.
    
    If a name in `columns_content` matches a name in `columns_common`, the name
    in `columns_content` is prefixed with `'content_'`. If still not unique,
    the name is suffixed with `_<number>` until a unique name is produced.
    """
    def generate_unique_name(name):
        yield f'content_{name}'
        i = 2
        while True:
            yield f'content_{name}_{i}'
            i += 1
    
    columns_common_set = set(columns_common)
    uniquified_columns_content = []
    
    for name in columns_content:
        uniquified_name = name
        unique_name_generator = generate_unique_name(name)
        while uniquified_name in columns_common_set:
            uniquified_name = next(unique_name_generator)
        
        uniquified_columns_content.append(uniquified_name)
    
    return columns_common + uniquified_columns_content


def get_match_query(input_name):
    """
    Return a query to match required fields for the specified input name.
    """
    query = {
        field.name: {'$exists': True}
        for field in fields.COMMON_FIELDS.values()
        if field.required and field.name != 'input'
    }
    
    query['input'] = input_name
    
    query.update({
        f'content.{field.name}': {'$exists': True}
        for field in fields.CONTENT_FIELDS_PER_INPUT[input_name].values()
        if field.required
    })
    
    return query


def _sort_columns(columns, fields_dict):
    known_columns = [
        column for column in columns if column in fields_dict]
    unknown_columns = [
        column for column in columns if column not in fields_dict]
    
    field_index_mapping = {
        field: index for index, field in enumerate(fields_dict)}
    
    known_columns.sort(key=lambda column: field_index_mapping[column])
    unknown_columns.sort()
    
    return known_columns + unknown_columns


def _safe_remove(list_, value):
    try:
        list_.remove(value)
    except ValueError:
        pass
