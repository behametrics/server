from datetime import datetime

from flask import Blueprint
from flask import request
from flask import Response
from flask import jsonify
from marshmallow import validate
from webargs import fields
from webargs.flaskparser import use_kwargs

from server.error import NoMatchingData
from server.services import data as data_service
from server.util import JsonQueryParser
from server.util import normalize_input_name
from server.util import only_logger

blueprint = Blueprint('data', __name__)


def _service():
    return data_service


@blueprint.route('', methods=['GET'])
def get_all():
    args = request.args.to_dict()
    return jsonify(_service().fetch_for_query(query=args))


@blueprint.route('', methods=['POST'])
@only_logger
def upload_data():
    _service().save_data(request.get_json())
    return '', 204


@blueprint.route('/csv', methods=['GET'])
@use_kwargs({
    'columns': fields.DelimitedList(
        fields.Str(validate=validate.Length(min=1)), required=True),
    'query': fields.Function(deserialize=JsonQueryParser(), missing=dict),
})
def get_csv(query, columns):
    return _create_csv_response(query, _parse_columns_list(columns))


def _parse_columns_list(columns_list, content_prefix='content.'):
    columns_common = [
        column for column in columns_list
        if not column.startswith(content_prefix)]
    columns_content = [
        column[len(content_prefix):] for column in columns_list
        if column.startswith(content_prefix)]

    return columns_common, columns_content


@blueprint.route('/csv/<input_name>')
@use_kwargs({
    'query': fields.Function(deserialize=JsonQueryParser(), missing=dict)
})
def get_csv_for_input(input_name, query):
    columns = _service().get_columns_for_input(input_name)
    
    if columns is None:
        raise NoMatchingData(input_name)
    
    query = _service().get_query_for_input(query, input_name)

    return _create_csv_response(query, columns, input_name)


def _create_csv_response(query, columns, input_name=None):
    creation_date = datetime.now().isoformat()
    
    if input_name is None:
        filename = f'export-{creation_date}.csv'
    else:
        normalized_input_name = normalize_input_name(input_name)
        filename = f'export-{normalized_input_name}-{creation_date}.csv'

    headers = {
        'Content-Disposition': f'attachment; filename={filename}'
    }

    generator = _service().create_csv(query, columns)
    
    return Response(
        generator,
        status=200,
        headers=headers,
        mimetype='text/csv')
