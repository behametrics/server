# Behametrics Server

This is a [Flask](https://flask.palletsprojects.com)-based server implementation allowing easy integration of loggers from the [Behametrics](https://gitlab.com/behametrics) project
and the [behalearn](https://gitlab.com/behametrics/behalearn) library.
The server implementation follows the [Behametrics specification](https://gitlab.com/behametrics/specification/blob/release/Specification.md).

## Features

* Uploading data from Behametrics loggers in JSON format
* Generating a session ID for loggers
* JSON-to-CSV data transformation

## Requirements

* Python 3.7 or later + dependencies from [requirements.txt](requirements.txt)
* MongoDB 4.0 or later

## Running the Server

To run the server, you have the following choices:
1. Clone the repository and run the Flask development server
   or deploy the server using [standard procedures](http://flask.palletsprojects.com/en/1.1.x/deploying/).
2. Build a Docker container using the provided [Dockerfile](Dockerfile).
3. Use the prebuilt Docker container available in the project's container registry: `registry.gitlab.com/behametrics/server`.
   * `latest` - latest stable build from the `release` branch
   * `latest-dev` - latest development build from the `master` branch
4. Use a [Docker Compose file](/dev/run/without-db/docker-compose.yml)
   that expects `MONGO_URI` for database connection and exposes endpoints on port 5000.
   See [Docker Compose](https://docs.docker.com/compose/gettingstarted/) for more information.
5. Use a [Docker Compose file](/dev/run/with-db/docker-compose.yml)
   that comes pre-installed with an internal MongoDB container (service) and exposes endpoints on port 5000.
   Using this configuration, you are able to run everything in one command, but [be cautious of your data](https://docs.docker.com/storage/).
   See [Docker Compose](https://docs.docker.com/compose/gettingstarted/) for more information.

For options 1-4, you are required to provide your own MongoDB database.

## Server Configuration

The server is configured via environment variables:

| Configuration entry    | Description                                                                                       | Default value                       |
|:-----------------------|:--------------------------------------------------------------------------------------------------|:------------------------------------|
| `DEBUG`                | If `True`, use the Flask debug configuration.                                                     | `True`                              |
| `PORT`                 | HTTP port.                                                                                        | `5000`                              |
| `MONGO_URI`            | URI used for connecting to a MongoDB instance.                                                    | `'mongodb://localhost/behametrics'` |
| `LOGGER_HEADER_NAME`   | Name of the header contained in POST requests sent from loggers.                                  | `'Logger'`                          |
| `LOGGER_HEADER_PREFIX` | Required prefix for the value of the header named `LOGGER_HEADER_NAME` in incoming POST requests. | `'behametrics/'`                    |

## API Documentation

* Swagger-generated API Documentation for the [latest official release](https://behametrics.gitlab.io/-/server/-/jobs/505835462/artifacts/public/index.html)

## License

Source code is provided under the [MIT License](LICENSE).
