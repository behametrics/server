# Contributing to Behametrics Server

Thank you for taking your time to contribute to Behametrics Server.
To help improve the project, we would like you to follow guidelines specified in this document.

## Submitting Changes

1. Make sure your proposed change (new feature, bug fix) is not already implemented (by searching the documentation, closed issues or merge requests).
2. Fork the repository.
3. Create a new, appropriately named branch:

       git checkout -b fix-get-session-id master

4. Commit your changes to the new branch.
5. Push your changes to the remote repository:

       git push origin fix-get-session-id

6. Create a new merge request on GitLab with `master` as the target branch.

## Getting Started

Once you fork the repository, you need to install the required dependencies.
Since the project requires specific versions of the dependencies,
it is recommended to use `virtualenv` to limit the dependencies to this project:
```
pip3 install virtualenv
cd <path to project>
virtualenv .
source ./bin/activate
pip3 install -r requirements.txt
```

## Coding Conventions

Follow the [PEP 8](https://www.python.org/dev/peps/pep-0008/) style guide.

### Tests

Any new feature or bug fix must be verified by unit tests.
