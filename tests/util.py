from unittest.mock import Mock


def patch_with_mock(monkeypatch, where, what):
    mock = Mock()
    monkeypatch.setattr(where, what, lambda: mock)
    return mock


def stub_based_on_arg_value(arg_check, return_value):
    def effect(*args, **kwargs):
        if arg_check(*args, **kwargs):
            return return_value
        return None

    return effect


def stub_generator(values):
    for i in values:
        yield i
