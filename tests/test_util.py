from json import dumps
from marshmallow import ValidationError
from webargs import fields

import pytest

from server.util import JsonQueryParser
from server.util import normalize_input_name


@pytest.mark.parametrize('data,parser,expected_result', [
    (dumps({'input': 'wheel'}),
     JsonQueryParser({'input': fields.Str()}),
     {'input': 'wheel'}),
    (dumps({'input': 'wheel', 'timestamp': 456}),
     JsonQueryParser({'input': fields.Str()}),
     {'input': 'wheel'}),
    (dumps({'timestamp': 456}),
     JsonQueryParser({'input': fields.Str(required=False)}),
     {}),
    ('{}',
     JsonQueryParser({'input': fields.Str(required=False)}),
     {}),
    ('{"input": "wheel"}', JsonQueryParser(),
     {'input': 'wheel'})
])
def test_json_query_parser(data, parser, expected_result):
    assert parser(data) == expected_result


@pytest.mark.parametrize('data,parser', [
    (dumps({'timestamp': 456}),
     JsonQueryParser({'input': fields.Str(required=True)})),
])
def test_json_query_parser_errors(data, parser):
    with pytest.raises(ValidationError):
        parser(data)


@pytest.mark.parametrize('input_name,expected_result', [
    ('name', 'name'),
    ('nAmE', 'name'),
    ('NAme', 'name'),
    ('NAME', 'name'),
])
def test_normalize_input_name(input_name, expected_result):
    assert (normalize_input_name(input_name) == expected_result)
