from server import views

from tests.util import patch_with_mock
from tests.util import stub_based_on_arg_value


def test_create_session(client, monkeypatch):
    sessions_dao_mock = patch_with_mock(
        monkeypatch, views.session, 'get_sessions_dao')
    data = {'sample': 'data'}

    sessions_dao_mock.save.return_value = {'_id': 'obj-id'}

    response = client.post(
        '/session', json=data, headers={'Logger': 'behametrics/web'})

    sessions_dao_mock.save.assert_called_once_with(data)
    assert response.get_json() == {'id': 'obj-id'}


def test_get_session_data(client, monkeypatch):
    sessions_dao_mock = patch_with_mock(
        monkeypatch, views.session, 'get_sessions_dao')
    
    session_id = '5c70579af2307d000b7eaf0c'
    expected_data = {
        '_id': session_id,
        'sample': 'data',
    }

    sessions_dao_mock.get_one.side_effect = stub_based_on_arg_value(
        arg_check=lambda arg: arg == {'_id': session_id},
        return_value=expected_data)

    response = client.get(f'/session/{session_id}')

    assert response.get_json() == expected_data
