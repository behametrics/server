import json
import pytest

from server import views
import server.services.data as service

from tests.util import patch_with_mock
from tests.util import stub_based_on_arg_value
from tests.util import stub_generator


def _mock_service(monkeypatch):
    return patch_with_mock(monkeypatch, views.data, '_service')


def test_correct_service_is_called():
    assert views.data._service() == service


@pytest.mark.parametrize('data', [
    {'input': 'wheel', 'timestamp': 456},
    [{'input': 'wheel', 'timestamp': 456},
     {'input': 'wheel', 'timestamp': 456}]
])
def test_save_data(data, client, monkeypatch):
    service_mock = _mock_service(monkeypatch)

    response = client.post(
        '/data', json=data, headers={'Logger': 'behametrics/web'})

    service_mock.save_data.called_once_with(data)
    assert response.status_code == 204


def test_save_data_without_header(client):
    response = client.post('/data', json={})
    assert response.status_code == 403


def test_get_data(client, monkeypatch):
    service_mock = _mock_service(monkeypatch)

    expected_data = {
        'session_id': '5c70579af2307d000b7eaf0c',
        'name': 'behametrics'
    }

    service_mock.fetch_for_query.return_value = expected_data

    query = {'session_id': '5c70579af2307d000b7eaf0c'}
    response = client.get('/data', query_string=query)
    result = json.loads(response.data)

    service_mock.fetch_for_query.assert_called_once_with(query=query)
    assert result == expected_data


@pytest.mark.parametrize('columns,parsed_columns,csv_rows', [
    (['input', 'timestamp'],
     (['input', 'timestamp'], []),
     ['input,timestamp', 'wheel,456', 'wheel,466']),
    (['input', 'timestamp', 'content.delta_x', 'content.delta_y'],
     (['input', 'timestamp'], ['delta_x', 'delta_y']),
     ['input,timestamp,delta_x,delta_y', 'wheel,456,10,', 'wheel,466,10,']),
])
def test_get_csv(columns, parsed_columns, csv_rows, client, monkeypatch):
    service_mock = _mock_service(monkeypatch)

    def create_csv_arg_check(query, columns):
        return query == {} and columns == parsed_columns

    service_mock.create_csv.side_effect = stub_based_on_arg_value(
        arg_check=create_csv_arg_check,
        return_value=stub_generator([row + '\n' for row in csv_rows]))
    
    response = client.get(
        '/data/csv',
        query_string={'columns': ','.join(columns)})
    
    expected_csv = '\n'.join(csv_rows) + '\n'

    assert response.headers['Content-Type'] == 'text/csv; charset=utf-8'
    assert str(response.data, encoding='utf-8') == expected_csv


def test_get_csv_without_columns(client, monkeypatch):
    response = client.get('/data/csv')
    
    assert response.status_code == 422
    assert response.get_json()['code'] == 'request/validation-failure'


def test_get_csv_for_input(client, monkeypatch):
    service_mock = _mock_service(monkeypatch)

    mongo_query = {'sample': 'query'}
    query_string = {'query': json.dumps(mongo_query)}
    
    csv_rows = ['input,timestamp', 'wheel,456', 'wheel,466']
    
    expected_result = '\n'.join(csv_rows) + '\n'
    generator = stub_generator([row + '\n' for row in csv_rows])

    def get_query_for_input_arg_check(query, input_name):
        return query == mongo_query and input_name == 'wheel'

    def generator_arg_check(query, columns):
        return query == mongo_query and columns == (['input', 'timestamp'], [])

    service_mock.get_query_for_input.side_effect = stub_based_on_arg_value(
        arg_check=get_query_for_input_arg_check,
        return_value=mongo_query)

    service_mock.get_columns_for_input.side_effect = stub_based_on_arg_value(
        arg_check=lambda input_name: input_name == 'wheel',
        return_value=(['input', 'timestamp'], []))

    service_mock.create_csv.side_effect = stub_based_on_arg_value(
        arg_check=generator_arg_check,
        return_value=generator)

    response = client.get('/data/csv/wheel', query_string=query_string)

    assert response.headers['Content-Type'] == 'text/csv; charset=utf-8'
    assert str(response.data, encoding='utf-8') == expected_result


def test_get_csv_for_input_no_matching_data(client, monkeypatch):
    service_mock = _mock_service(monkeypatch)

    service_mock.get_columns_for_input.return_value = None

    response = client.get('/data/csv/wheel')

    assert response.status_code == 422
    assert response.get_json()['code'] == 'request/no-matching-data'
