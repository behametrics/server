import pytest

from server import create_app


@pytest.fixture
def client():
    return create_app().test_client()
